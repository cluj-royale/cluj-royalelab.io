---
title: Cerințe minime
comments: false
tags: [clash royale, reguli de citit când ești pe fugă]
---

<i class="fa fa-fort-awesome fa-2x"></i> [#G8LUG9](https://royaleapi.com/clan/G8LUG9) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<i class="fa fa-graduation-cap fa-2x"></i> [#2U9GY8LQ](https://royaleapi.com/clan/2U9GY8LQ) (pentru condițiile specifice academiei puteți să vizitațti [site-ul academiei](https://clujacademy.wordpress.com/regulament/))

## Condiții:

 - Minim 4600 de cupe (numărul de cupe fluctuează în timp, în funcție de nr. de membri în clan)
 - Minim 35 war day wins
 - Minim nivelul 12 la turnul regelui (King Tower) (am impus această regulă deoarece am observat că membrii sub nivelul 12 au șanse mai mici de câștig în WD)
 - Cerere diferită de default (dacă ai interesul de a intra în clan, vei avea interesul și de a-ți lua câteva secunde să modifici textul clasic)

**Notă:** Este în interesul tuturor jucătorilor să se bucure cât mai mult de joc indiferent de condiții. Clanul [Cluj Royale](/) nu tolerează comportamentul vulgar și abuziv la adresa altor jucători (indiferent de clan, sex, rasă sau religie). Acest comportament se pedepsește cu report și kick:

- Nume vulgar
- Ești membru vechi care nu a respectat regulamentul clanului
- Cererea conține cuvinte de natură vulgară/jignitoare
- Comportament vulgar sau abuziv la adresa membrilor clanului sau a altor jucători

## Ieșirea temporară din clan

Ieșirea temporară din clan cu redobânirea rangului avut înainte de ieșire se face **doar cu acordul unui co-leader sau leader**. Pentru detalii și condiții [consulțati regulamentul](/page/rules).

## Donații

Cluj Royale are o politică strictă legată de activitate și donații:

- o perioadă de inactivitate mai lungă de 2 săptamâni, fără a fi acceptata de un co-leader sau leader este sancționată
- cei sub 100 de donații saptămânale au șanse să primească demote/kick

## Condiții de promovare 

### Elder:

- minim 450 de donații saptămânale
- nu au fost avertizați pentru nerespectarea altor reguli

### Co-lead:

În general, leaderul și co-leaderii monitorizează diferiți jucători pentru promovare o perioadă mai îndelungată de timp. Pentru mai multe detalii vezi [regulamentul](/page/rules).

### Leader: 

În condiții excepționale leaderul poate fi schimbat prin votul co-leaderilor.