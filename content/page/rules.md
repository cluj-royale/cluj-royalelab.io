---
title: Regulile clanului
comments: false
tags: [clash royale, reguli generale, war day]
---

# Reguli generale

În intenția de a susține atmosfera de prietenie, susțiem ideea că fiecare membru, indiferent de rang, trebuie să respecte pe ceilalți membri ai clanului, să aducă critică dar doar în formă constructivă (fără jigniri, limbaj vulgar sau asemănătoare) în scopul păstrării unui tot unitar, pentru fidelizarea membrilor dar și pentru devoltare comună.

Fiecare membru este liber să plece voluntar, indiferent de motiv. În cazul în care un membru a părăsit clanul adresând injurii la adresa altor membrii ai clanului, sau nu a motivat părăsirea, are șanse mari să nu mai fie din nou acceptat în clan.

Poți invita prieteni în clan, cu condiția ca și aceștia să îndeplinească condițiile de intrare în clan (vezi [conditii minime](/page/rules-short)).

Acceptarea unui membru care nu îndeplinește condițiile clanului duce automat la demote/kick.

Începând cu update-ul de pe 5 septembrie 2018, emotes pot fi folosite și în chat-ul clanului. Recomandăm utilizarea decentă a acestei funcții, deoarece orice formă (și cea clasică, de tip text) de spam va fi sancționată.

## Condiții generale de promovare

Odată ce ai intrat în clan, ai responsabilitatea de a îndeplini câteva cerințe, pentru a-ți oferi propria contribuție clanului, așa cum și clanul te va răsplăti pentru participare activă. Acestea sunt:

- Minim 450 de donații pe săptămână
- Socializare pe chat-ul clanului (opțional)
- Participare la Clan Wars (opțional dar recomandat)

Îndeplinirea acestor cerințe ajută și la promovarea în rang în clan, așa că este la latitudinea ta dacă intenționezi să devii un membru activ al clanului sau nu.

## Ieșirea temporară din clan

Ieșirea temporară din clan cu redobânirea rangului avut înainte de ieșire se face **doar cu acordul unui co-leader sau leader**:

- Promovarea la titlul avut înainte de ieșirea din clan nu este obligatorie, mai ales dacă acest lucru se întâmplă frecvent. 
- Jucatorul care își dă acordul este responsabil pentru promovarea jucătorului ieșit temporar din clan. Dacă jucatorul în cauză spamează elderii, co-leaderii sau leaderul atunci acesta nu va mai fi invitat în clan și nu va redobandi titlul avut înainte de ieșire.
- Dacă jucătorul a ieșit din clan pe motivul unui abuz, acesta poate fi invitat înapoi fie de un elder, co-leader sau leader.

## Sancțiuni

În clanul nostru, sancțiunile se aplică prin demote/kick, săptămânal, în cazul în care nu sunt respectate cerințele.

Spre exemplu, dacă în săptămâna 1 ești elder, dar nu îți îndeplinești donațiile săptămânale, vei primi automat demote la sfârșitul săptămânii. Dacă nici în decursul săptămânii 2 nu îți vei îndeplini numărul minim de donații, vei primi automat kick (deoarece te vom considera membru inactiv).

Excepție de la această regulă o fac cazurile de comportament neadecvat, în cazul în care se adresează injurii la adresa altor membri ai clanului (fapt ce se sancționează cu kick instant).

## Comportament abuziv

Este în interesul tuturor jucătorilor să se bucure cât mai mult de joc indiferent de condiții. Clanul [Cluj Royale](/) nu tolerează comportamentul vulgar și abuziv la adresa altor jucători (indiferent de clan, sex, rasă sau religie). Acest comportament se pedepsește cu report și kick:

- Nume vulgar
- Ești membru vechi care nu a respectat regulamentul clanului
- Cererea conține cuvinte de natură vulgară/jignitoare
- Comportament vulgar sau abuziv la adresa membrilor clanului sau a altor jucători

# Clan Wars

## Prep Day

Ai 3 jocuri disponibile, ce vor aduce cărți clanului și inclusiv ție. Ai un tip de joc preferat la care ești sigur că o să câștigi? Ai răbdare, așteaptă să revină pe hartă pentru a-l juca din nou; până la urmă sunt disponibile 24 de ore până la War Day. Fă tot posibilul să îți dai cele 3 atacuri, pentru a obține cât mai multe cărți pentru clan. Chiar dacă pierzi, nu e nicio problemă, un atac pierdut e mai bun decât niciun atac.

## War Day

În cea mai importantă parte a Clan Wars, fiecare membru participant are obligația de ataca în War Day, cu scopul de a câștiga și de a aduce puncte clanului. Nu penalizăm atacurile pierdute la war, dar e de la sine înțeles că un atac câștigat oferă mai multe resurse câștigătorilor. Nu ești sigur de deck-ul pe care vrei să-l utilizezi în War Day? Aruncă o privire pe chat, poate găsești un deck bun de la membrii participanți, sau măcar să îți ofere destulă inspirație.

## Sancțiuni

Sancțiunile sunt asemănătoare regulamentului principal, doar motivele sunt diferite.

Astfel, jucătorii ce nu își utilizează toate atacurile în Preparation Day au șansa de a primi demote. Atacul la War Day este obligatoriu. Astfel, dacă nu vei ataca, vei avea garantat demote/kick la finalul warului. De asemenea, dacă la War Day, prin utilizarea unui deck de tip troll (care clar nu asigură victoria) sau comportamentul de tip troll pe perioada atacului, se va considera atitudine incorectă față de war și lipsă de respect față de ceilalți membri participanți, fiind sancționată acțiunea cu kick.

# Promovare

Cea mai simplă și mai populară formă de promovare o reprezintă trecerea de la membru la elder. Odată ce ai îndeplinit cerințele de promovare, ești eligibil pentru a deveni elder:

### Elder:

- minim 450 de donații saptămânale
- nu au fost avertizați pentru nerespectarea altor reguli

### Co-lead:

În general, leaderul și co-leaderii monitorizează diferiți jucători pentru promovare o perioadă mai îndelungată de timp. Pentru a deveni co-lider însă, este necesară o serie de criterii pentru a face acest lucru. Pe lângă donațiile săptămânale, mai este necesar:

- activitate și socializare pe chat-ul clanului
- performațe la Clan Wars
- experiență
- vechime în clan

### Leader: 

În condiții excepționale leaderul poate fi schimbat prin votul co-leaderilor.