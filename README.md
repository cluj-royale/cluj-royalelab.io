# Building locally

To work locally with this project, you'll have to follow the steps below:

1. Clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content

Read more at Hugo's [documentation][].

# Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.
